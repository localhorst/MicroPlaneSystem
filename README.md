# MicroPlaneSystem

## Overview

![](https://git.mosad.xyz/localhorst/MicroPlaneSystem/raw/commit/652460d0e11f890a7a38f7d2a6e7312ea20e61b4/Hardware/Platine/front)

* 5 Servo Outputs (3.3V or 5V)
* 2x Brushed ESC 7A
* 1S or 2S LiPo Input (select via jumper)
* PPM, Serial or S-Bus receiver input (see more in receiver sections)
* 2x LED Outputs (onboard and extern) 1A @ 3.3V
* Battery voltage detections (low battery warning)
* MPU6050 support for stabilization
* Expansion connector

## Hardware

### Receivers

todo

### Servos

todo

### Motors

todo

### Batteries

todo

### LEDs

todo

### Stabilization
todo

### Extras

todo 

## Software

todo



